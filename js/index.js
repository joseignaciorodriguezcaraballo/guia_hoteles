$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel').carousel({
    			interval: 1000
    		});
    	
    	    $('#contacto').on('show.bs.modal', function (e){
    		console.log('el modal contacto se esta mostrando');

    		$('#contactoBtn').removeClass('btn btn-outline-success');
    		$('#contactoBtn').addClass('btn-secondary');
    		$('#contactoBtn').prop('disable', true);

    	   });
    	   $('#contacto').on('shown.bs.modal', function (e){
    		console.log('el modal contacto se mostro');

    	   });
    	   $('#contacto').on('hide.bs.modal', function (e){
    		console.log('el modal contacto se oculta');
    	   });
    	   $('#contacto').on('hidden.bs.modal', function (e){
    		console.log('el modal contacto se oculto');
    	   
           $('#contactoBtn').prop('disable', false);
    	});

    	});